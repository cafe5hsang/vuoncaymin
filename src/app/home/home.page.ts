import { Component } from '@angular/core';
import * as firebase from 'Firebase';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  timeStart: string = "00:00";
  timeEnd: string = "00:00";

  sensors: any = {
    light: 0,
    temperature: 0
  };

  setting: any = {
    manual: 0,
    temperature: 30,
    light: 1,
    timer: 0,
    timeStart: "00:00",
    timeEnd: "00:00"
  };

  relays = [];
  relayRef = firebase.database().ref('relay/');

  constructor() {
    this.relayRef.on('value', resp => {
      this.relays = [];
      this.relays = snapshotToArray(resp);
      console.log(this.relays);
    });

    firebase.database().ref('sensor').on('value', resp => {
      let sensors: any = snapshotToObject(resp);
      this.sensors.light = sensors.light;
      this.sensors.temperature = sensors.temperature;
      console.log(this.sensors);
    });

    firebase.database().ref('setting').on('value', resp => {
      let setting: any = snapshotToObject(resp);
      this.setting.light = setting.light;
      this.setting.temperature = setting.temperature;
      this.setting.manual = setting.manual;
      this.setting.timeStart = setting.time_start;
      this.setting.timeEnd = setting.time_end;
      console.log(this.setting);
    });
  }

  public updateRelayStatus(key: string, value: number): void {
    console.log('key: ' + key);
    console.log('value: ' + value);
    firebase.database().ref('relay/' + key).set(value == 1 ? 0 : 1);
  }

  public updateSetting(key: string, value: number): void {
    console.log('key: ' + key);
    console.log('value: ' + value);
    if (key == 'manual' || key == 'light') {
      firebase.database().ref('setting/' + key).set(value == 1 ? 0 : 1);
      this.setting[key] = value == 1 ? 1 : 0;
    } else {
      firebase.database().ref('setting/' + key).set(value);
      this.setting[key] = value;
    }
  }

  public updateTime(key: string, value: string) {
    firebase.database().ref('setting/' + key).set(value);
  }

}

export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item: any = {}; 
      item.value = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });

  return returnArr;
}

export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;

  return item;
}
