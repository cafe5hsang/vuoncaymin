import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyDdOuPnE-qpblkMiJme0vZulRFRI6OKZcE",
    authDomain: "vuoncaymin.firebaseapp.com",
    databaseURL: "https://vuoncaymin.firebaseio.com",
    projectId: "vuoncaymin",
    storageBucket: "vuoncaymin.appspot.com",
    messagingSenderId: "613089660615",
    appId: "1:613089660615:web:55912de617de725d"
};

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    firebase.initializeApp(config);
  }
}
